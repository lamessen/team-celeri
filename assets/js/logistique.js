$(document).ready(function() {
    $('#myModal').on('shown.bs.modal', function () {
        $('#myModal video')[0].play();
    });
    $('#myModal').on('hidden.bs.modal', function () {
        $('#myModal video')[0].pause();
    });
});
