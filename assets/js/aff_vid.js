let modal = null

const openModal = function(e) {
    alert('OUVERT')
    e.preventDefault()
    const target = document.querySelector(e.target.getAttribute('href'))
    target.removeAttribute('display')
    target.removeAttribute('aria-hidden')
    modal = target
    modal.addEventListener('click', closeModal)
}

const closeModal = function(e) {
    if (modal == null) return
    e.preventDefault()
    modal.style.display = 'none'
    modal.setAttribute('arria-hiden', 'true')
    modal.removeEventListener('click', closeModal)
    modal.querySelector('js-modal-close').removeEventListener('click', closeModal)
    modal = null
}

document.querySelectorAll('.js-modal').forEach(a => {
    a.addEventListener('click', openModal)
})